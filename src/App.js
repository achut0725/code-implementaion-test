import { Outlet, RouterProvider, createBrowserRouter } from 'react-router-dom';
import ReactDOM from "react-dom/client";
import './App.css';
import Body from './Components/Body.tsx';
// import Header from './Components/Header';
// import SideBar from './Components/SideBar';
// import Header from './Components/Header';
// import SideBar from './Components/SideBar';
// import Body from './Components/Body';

const appRouter = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {path: "/", element: <Body />}
    ]
  }
])

function App() {
  return (
    <>

        {/* <Header />
        <SideBar /> */}
        <Outlet />

    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(<RouterProvider router={appRouter} />);