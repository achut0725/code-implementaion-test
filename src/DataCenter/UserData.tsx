export const UserData = [
  { id: 1, year: 2010, usersGain: 56588, usersLost: 548 },
  { id: 2, year: 2011, usersGain: 43210, usersLost: 876 },
  { id: 3, year: 2012, usersGain: 78965, usersLost: 234 },
  { id: 4, year: 2013, usersGain: 34567, usersLost: 654 },
  { id: 5, year: 2014, usersGain: 87654, usersLost: 321 },
  { id: 6, year: 2015, usersGain: 12345, usersLost: 987 },
];
