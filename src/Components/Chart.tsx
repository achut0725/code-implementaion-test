import React from "react";

import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
} from "chart.js";

ChartJS.register(LineElement, CategoryScale, LinearScale, PointElement);

function Chart() {
  const data = {
    labels: ["Mar 12", "", "Mar 13", "", "Mar 14", "", "Mar 15", "", "Mar 16", "", "Mar 17"],
    datasets: [
      {
        data: [8, 7.8, 7.2, 6, 8.5, 9.5, 7, 6.5, 5, 4],
        backgroundColor: "transparent",
        borderColor: "#f26c6d",
        // pointBorderColor: "transparent",
        pointBorderWidth: 4,
        tension: 0.5
      },
    ],
  };

  const options = {
    plugins: {
      legend: false,
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        min: 0,
        max: 10,
        ticks: {
          stepSize: 2,
          callback: (value)=> value + "K"
        },
        grid: {
          borderDash: [10],
        },
      },
    },
  };
  return (
    <div styles={{ width: "500px", height: "500px", marginLeft: "20px" }}>
      <Line data={data} options={options}></Line>
    </div>
  );
}

export default Chart;
