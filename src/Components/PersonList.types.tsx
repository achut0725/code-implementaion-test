export type Name = {
  firstname: string;
  lastname: string;
};

export type PersonListProps = {
  namesList: Name[][];
};
