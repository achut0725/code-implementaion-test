import React from "react";
import { PersonListProps } from "./PersonList.types";

function PersonList({ namesList }: PersonListProps) {
  return (
    <>
      <div>
        {namesList.map((names) => {
          return (
            <div>
              {names.map((name) => {
                return (
                  <div>
                    <h1>
                      {name.firstname} {name.lastname}
                    </h1>
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </>
  );
}

export default PersonList;


//* correct and tested