import React from "react";

type HeadingProps = {
  children: String;
};

function Heading(props: HeadingProps) {
  return (
    <>
      <h1>{props.children}</h1>
      <h1 title={"this is a tittile"} contenteditable = "true" >helooo</h1>
      <p marqee="right"></p>

      <marquee direction="left">this is the animation</marquee>
    </>
  );
}

export default Heading;
