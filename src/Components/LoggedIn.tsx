import React, { useState } from "react";

export const LoggedIn = () => {
  const [isLoggedIn, setIsloggedIn] = useState(false);

  const handleLogin = () => {
    setIsloggedIn(true);
  };

  const handleLogout = () => {
    setIsloggedIn(false);
  };

  return (
    <div>
      <button onClick={handleLogin}>Login</button>
      <button onClick={handleLogout}>logout</button>
      <h3>{isLoggedIn ? "User is logged in" : "User logged out"}</h3>
    </div>
  );
};
