import React from 'react'

type GreetProps = {
    name: String,
    isLoggedIn: Boolean,
    messageCount?: Number,
}

function Greet(props: GreetProps) {
    const {messageCount = 0} = props
  return (
    <div>
        <h2>
            {props.isLoggedIn ?
            `Welcome ${props.name} ! you have ${messageCount} unreaded messages` : "Welcome guest"}
        </h2>
    </div>
  )
}

export default Greet