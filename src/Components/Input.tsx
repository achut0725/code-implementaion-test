import React from 'react'

type InputProps = {
    value: String,
    handleChange: (event: React.ChangeEvent<HTMLInputElement>)=> void
}

function Input(props: InputProps) {
    const handleInputChange =(event: React.ChangeEvent<HTMLInputElement>)=> {
        console.log(event)
    }
  return (
    <input type='text' value={props.value} onChange={props.handleChange}/>
  )
}

export default Input