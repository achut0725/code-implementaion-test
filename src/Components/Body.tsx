import React, { useState } from "react";
import Status from "./status";
import Parent from "./Parent";
import Child from "./Child";
import Greet from "./Greet";
import { Button } from "./Button";
import Input from "./Input";
import { Container } from "./Container";
import PersonList from "./PersonList";
import { LoggedIn } from "./LoggedIn";
import { User } from "./Users";
import { Counter } from "./Counter";
import Chart from "./Chart";
import BarChart from "../Charts/BarChart";
import { UserData } from "../DataCenter/UserData";
import PieChart from "../Charts/PieChart";

function Body() {
  const nameList = [
    [
      { firstname: 'John', lastname: 'Doe' },
      { firstname: 'Jane', lastname: 'Smith' },
      { firstname: 'David', lastname: 'Johnson' }
    ],
    [
      { firstname: 'Sarah', lastname: 'Williams' },
      { firstname: 'Michael', lastname: 'Brown' },
      { firstname: 'Emily', lastname: 'Davis' }
    ],
    [
      { firstname: 'Alex', lastname: 'Wilson' },
      { firstname: 'Olivia', lastname: 'Taylor' },
      { firstname: 'Daniel', lastname: 'Thomas' }
    ]
  ];

  const [userData, setUserData] = useState({
    labels: UserData.map((user)=> user.year),
    datasets: [{
      label: "Users Gain",
      data: UserData.map((user)=> user.usersGain),
      backgroundColor: ["blue", "purple", "green", "orange"],
    }],
  })

  return (
    <>
      <div className="body">
        <div>
          <div className="section1">
            <h1>hello</h1>

            {/* <BarChart chartData={userData}/> */}

            <PieChart chartData={userData} />

            <Chart />

          </div>
        </div>
      </div>
    </>
  );
}

export default Body;
