import React, { useState } from "react";

type AuthUser = {
  name: string;
  email: string;
};

export const User = () => {
//   const [user, setUser] = useState<AuthUser | null>(null);

    const [user, setUser] = useState<AuthUser>({} as AuthUser);

  const handleLogin = () => {
    setUser({
      name: "Aswin",
      email: "Aswin@gmail.com",
    });
  };

  const handleLogout = () => {
    setUser(null);
  };

  return (
    <div>
      <button onClick={handleLogin}>Login</button>
      <button onClick={handleLogout}>logout</button>
      <h3>User name is {user?.name}</h3>
      <h3>Email name is {user?.email}</h3>
    </div>
  );
};
