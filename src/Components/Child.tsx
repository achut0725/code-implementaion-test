import React from 'react'

type ParentProps = {
    children: React.ReactNode
}

function Child(props: ParentProps) {
  return (
    <div>{props.children}</div>
  )
}

export default Child