import React from 'react'

type ConteinerProps = {
    styles: React.CSSProperties;
}

export const Container =(props: ConteinerProps)=> {
  return (
    <div style={props.styles}>
        Text contents goes here
    </div>
  )
}
